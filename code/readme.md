## Directories

# EDD

Contains the Java files for implementing the EDD-based algorithm and a
running time file which executes a running time experiment on some
input data which can be randomly generated from the Data directory.

# SPT

Contains the Java files for implementing the SPT-based algorithm and a
running time file which executes a running time experiment on some
input data which can be randomly generated from the Data directory.

# Data

Contains the Java files used to randomly generate input data for testing
the running time and correctness of the SPT-based and EDD-based algorithms.

# Testing

Contains copies of the EDD and SPT source code files along with a verification
test file, comparing the validity of the schedules output by both algorithms
and whether they acquire the same result of minimising the same number
of late jobs.

Individual readme files can be found in each of the directories and
explanations for the source code are given in the relevant Java files
accordingly.
