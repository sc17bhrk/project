public class Job {
  int number;
  int proc_time;
  int due_date;

  public Job(int n, int p, int d)
  {
    this.number = n;
    this.proc_time = p;
    this.due_date = d;
  }

  public Job(int p, int d) {
    this(-1, p, d);
  }

  public Job(int d) {
    this(-1, -1, d);
  }
}
