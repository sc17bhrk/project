/*
Merge-sort is an efficient sorting algorithm that requires O(nlogn) time to sort an array of n numbers. Using the ideas of divide-and-conquer, merge-sort splits the array into two halves. Then it calls recursively itself in order to sort each half. The sorted halves are then merged together into a sorted array.
*/
public class MergeSort
{
  /*
  The algorithm sorts a given array by recursive merge-sort
  Input: an array arr[0..n-1] of n elements (Job objects)
  Output: array arr[0..n-1] sorted in ascending order according due dates
  */
  public static void sort_EDD(Job[] arr) {
    int n = arr.length;
    if (n > 1) {
      // find the middle index of the array
      int m = n/2;
      Job[] sub1, sub2;
      sub1 = new Job[m];
      sub2 = new Job[n - m];
      // Copy first half of arr into sub1
      System.arraycopy(arr, 0, sub1, 0, m);   //divide
      // Copy second half of arr into sub2
      System.arraycopy(arr, m, sub2, 0, n - m);    //divide
      sort_EDD(sub1);    //recur
      sort_EDD(sub2);    //recur
      merge_EDD(sub1, sub2, arr);    //conquer
    }
  }

  /*
  Iterative function merge in algorithm mergeSort merges two sorted arrays sub1 and sub2
  by selecting the smallest element from these arrays and adding it to the
  beginning of the output array arr.
  Then the second smallest element is selected from sub1 and sub2 and added to arr.
  These steps are continued until one of the two arrays sub1 or sub2 is empty.
  Then the remainder of the other array is copied into the output array arr.
  */
  public static void merge_EDD(Job[] sub1, Job[] sub2, Job[] arr) {
    /*
    Merges two sorted arrays into one sorted array
    Input: Arrays sub1[0..p-1] of p elements
    and sub2[0..q-1] of q elements both sorted
    Output: Sorted array arr[0..p+q-1] of the elements of sub1 and sub2
    */
    int i, j, k, p, q;
    i = 0;
    j = 0;
    k = 0;
    p = sub1.length;
    q = sub2.length;

    while (i < p && j < q) {
      // filling in sorted array arr based on ascending order of due dates
      if (sub1[i].due_date <= sub2[j].due_date) {
        arr[k] = sub1[i];
        ++i;
      }
      else {
        arr[k] = sub2[j];
        ++j;
      }
      ++k;
    }
    // we've filled in all elements of sub1 into arr, so copy the rest of sub2 into arr
    if (i == p) {
      System.arraycopy(sub2, j, arr, k, q-j);
    }
    // we've filled in all elements of sub2 into arr, so copy the rest of sub1 into arr
    else {
      System.arraycopy(sub1, i, arr, k, p-i);
    }
  }

  /*
  The algorithm sorts a given array by recursive merge-sort
  Input: an array arr[0..n-1] of n elements (Job objects)
  Output: array arr[0..n-1] sorted in ascending order according to processing time
  */
  public static void sort_SPT(Job[] arr) {
    int n = arr.length;
    if (n > 1) {
      // find the middle index of the array
      int m = n/2;
      Job[] sub1, sub2;
      sub1 = new Job[m];
      sub2 = new Job[n - m];
      // Copy first half of arr into sub1
      System.arraycopy(arr, 0, sub1, 0, m);   //divide
      // Copy second half of arr into sub2
      System.arraycopy(arr, m, sub2, 0, n - m);    //divide
      sort_SPT(sub1);    //recur
      sort_SPT(sub2);    //recur
      merge_SPT(sub1, sub2, arr);    //conquer
    }
  }

  /*
  Iterative function merge in algorithm mergeSort merges two sorted arrays sub1 and sub2
  by selecting the smallest element from these arrays and adding it to the
  beginning of the output array arr.
  Then the second smallest element is selected from sub1 and sub2 and added to arr.
  These steps are continued until one of the two arrays sub1 or sub2 is empty.
  Then the remainder of the other array is copied into the output array arr.
  */
  public static void merge_SPT(Job[] sub1, Job[] sub2, Job[] arr) {
    /*
    Merges two sorted arrays into one sorted array
    Input: Arrays sub1[0..p-1] of p elements
    and sub2[0..q-1] of q elements both sorted
    Output: Sorted array arr[0..p+q-1] of the elements of sub1 and sub2
    */
    int i, j, k, p, q;
    i = 0;
    j = 0;
    k = 0;
    p = sub1.length;
    q = sub2.length;

    while (i < p && j < q) {
      // filling in sorted array arr based on ascending order of processing time
      if (sub1[i].proc_time <= sub2[j].proc_time) {
        arr[k] = sub1[i];
        ++i;
      }
      else {
        arr[k] = sub2[j];
        ++j;
      }
      ++k;
    }
    // we've filled in all elements of sub1 into arr, so copy the rest of sub2 into arr
    if (i == p) {
      System.arraycopy(sub2, j, arr, k, q-j);
    }
    // we've filled in all elements of sub2 into arr, so copy the rest of sub1 into arr
    else {
      System.arraycopy(sub1, i, arr, k, p-i);
    }
  }
}
