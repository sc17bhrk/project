import java.util.Scanner;
import java.util.Arrays;
import java.util.NoSuchElementException;

/**
Class MaxBinaryHeap implements a maximum binary heap storing job objects
with the root containg the job with the largest processing time.
For any node, its children contain jobs with processing time smaller or
equal to its own job.
Used to miplement the priority queue.
**/
class MaxBinaryHeap
{
  // The number of children each node has
  private static final int d = 2;
  private int heapSize;
  private Job[] heap;

  // Constructor
  public MaxBinaryHeap(int capacity) {
    heapSize = 0;
    heap = new Job[capacity];
    Arrays.fill(heap, new Job(0,100000000,100000000));
  }

  // getters
  public Job[] getArray( ) {
    return heap;
  }

  // Function to check if heap is empty
  private boolean isEmpty( ) {
    return heapSize == 0;
  }

  // Check if heap is full
  private boolean isFull( ) {
    return heapSize == heap.length;
  }

  // Function to  get index parent of i
  private int parent(int i) {
    return (i - 1)/2;
  }

  // Function to get index of k th child of i
  private int kthChild(int i, int k) {
    return 2 * i + k;
  }

  // Function to insert element */
  public void insert(Job x) {
    if (isFull( ) )
    throw new NoSuchElementException("Overflow Exception");
    // toggle up
    heap[heapSize++] = x;
    toggleUp(heapSize - 1);
  }

  // Function to delete max element
  public Job deleteMax() {
    Job keyItem = heap[0];
    delete(0);
    return keyItem;
  }

  // Function to delete element at an index
  private Job delete(int ind) {
    if (isEmpty() )
    throw new NoSuchElementException("Underflow Exception");
    Job keyItem = heap[ind];
    heap[ind] = heap[heapSize - 1];
    heapSize--;
    toggleDown(ind);
    return keyItem;
  }

  // Function toggleUp
  private void toggleUp(int childInd) {
    Job tmp = heap[childInd];
    while (childInd > 0 && tmp.proc_time > heap[parent(childInd)].proc_time)
    {
      heap[childInd] = heap[parent(childInd)];
      childInd = parent(childInd);
    }
    heap[childInd] = tmp;
  }

  // Function toggleDown
  private void toggleDown(int ind) {
    int child;
    Job tmp = heap[ind];
    while (kthChild(ind, 1) < heapSize)
    {
      child = maxChild(ind);
      if (heap[child].proc_time > tmp.proc_time)
      heap[ind] = heap[child];
      else
      break;
      ind = child;
    }
    heap[ind] = tmp;
  }

  // Function to get largest child
  private int maxChild(int ind) {
    int bigChild = kthChild(ind, 1);
    int k = 2;
    int pos = kthChild(ind, k);
    while ((k <= d) && (pos < heapSize))
    {
      if (heap[pos].proc_time > heap[bigChild].proc_time)
      bigChild = pos;
      pos = kthChild(ind, k++);
    }
    return bigChild;
  }

  // Function to print heap
  public void printHeap() {
    System.out.print("\n*********************************\n");
    System.out.print("\nHeap:\n");
    int j = 0;
    int level = log2(heapSize);
    for (int i = 0; i < heapSize; i++) {
      if (i == Math.pow(2, j) - 1) {
        j++;
        System.out.println();
        System.out.print(EDD.padRight(" ", (int) 10 * (level - j + 1)));
      }
      String node = String.format("[j:%d, p:%d, d:%d]",
        heap[i].number, heap[i].proc_time, heap[i].due_date
      );
      System.out.print(node + " ");
      // padding between children of different parents
      if (i % 2 == 0) {
        System.out.print("   ");
      }
    }
    System.out.print("\n*********************************\n");
    System.out.println();
  }

  static int log2(int n) {
    return (int) (Math.log(n) / Math.log(2));
  }
}
