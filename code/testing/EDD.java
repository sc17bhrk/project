import java.util.*;
// EDD class implements EDD(Moore-Hodgson) algorithm
/*
Sort the array of jobs in increasing order of due date
Cycle through each of the jobs and add it to the partial schedule
 - this is a binary heap where the root is the maximum processing time for the job
If the current job added doesn't cause the machine to be tardy, i.e. over the due date
add it to the schedule,
otherwise, remove the job from the schedule which has the largest processing time
(the root of the binary heap)
schedule jobs in due date order and then tardy jobs in any order
*/
public class EDD {
  private Job[] jobs;
  private MaxBinaryHeap schedule;

  // constructor
  public EDD(int[] processing_times, int[] due_dates) {
    jobs = new Job[due_dates.length];
    // initialisng jobs with the job number, processing times and due dates
    for (int i = 0; i < jobs.length; ++i) {
      jobs[i] = new Job(i+1, processing_times[i], due_dates[i]);
    }
    schedule = new MaxBinaryHeap(jobs.length);
  }
  // run EDD(Moore-Hodgson) algorithm
  public Job[] run() {
    // reorder jobs in increasing order for due dates with merge sort
    MergeSort.sort_EDD(jobs);

    // this keeps track of the current overall processing time to check for tardiness
    int total_time = 0;
    for (Job j : jobs) {
      schedule.insert(j);
      // the current job does not make a job tardy
      if (total_time + j.proc_time <= j.due_date) {
        total_time += j.proc_time;
      }
      // we have a tardy job remove the one with longest processing time
      else {
        Job job_to_remove = schedule.deleteMax();
        total_time = total_time + j.proc_time - job_to_remove.proc_time;
        // mark deleted job as tardy
        job_to_remove.number = -job_to_remove.number;
      }
    }
    // sort the jobs in the schedule according to EDD order
    MergeSort.sort_EDD(jobs);

    // print the on time jobs in order and late jobs
    // ----- uncomment lines below to print schedule
    // System.out.println();
    // printValidJobs();
    // printInvalidJobs();
    // System.out.println();
    // -----
    return jobs;
  }

  // prints the jobs in their current state
  public void printJobs() {
    for (Job j : jobs) {
      String job = j.number + " " + j.proc_time + " " + j.due_date;
      System.out.print(job + ",   ");
    }
    System.out.println();
  }

  // prints all the jobs that are on time - i.e. have positive job number values
  public void printValidJobs() {
    System.out.println("On Time Jobs Schedule Order\n");
    System.out.println(padRight("Number", 10) + padRight("Proc Time", 12) +
                       padRight("Due Date", 12));
    for (Job j : jobs) {
      if (j.number > 0) {
        System.out.println(padRight(String.valueOf(j.number), 10) +
                           padRight(String.valueOf(j.proc_time), 12) +
                           padRight(String.valueOf(j.due_date), 12));
      }
    }
    System.out.println();
  } // printValidJobs() ends here

// prints all tardy jobs that are on time - i.e. have negative job number values
  public void printInvalidJobs() {
    System.out.println("Late Jobs");
    for (Job j : jobs) {
      if (j.number < 0) {
        System.out.println(padRight(String.valueOf(-j.number), 10) +
        padRight(String.valueOf(j.proc_time), 12) +
        padRight(String.valueOf(j.due_date), 12));
      }
    }
    System.out.println();
  } // printInvalidJobs() ends here

  // pads a string s with spaces to length n
  public static String padRight(String s, int n) {
    if (n == 0) {
      return String.format("");
    }
    return String.format("%-" + n + "s", s);
  } // padRight() ends here

  // main method - used for testing EDD algorithm
  public static void main(String[] args) {
    // test data to run - can modify these values
    int[] p = {10, 3, 4, 8, 10, 6};
    int[] d = {15, 6, 9, 23, 20, 30};
    // int[] p = {3, 4, 10, 10, 8, 6, 16, 12, 9, 13};
    // int[] d = {6, 9, 15, 20, 23, 30, 36, 40, 51, 57};

    // int[] p = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    // int[] d = {115, 99, 161, 112, 170, 155, 60, 85, 160, 112, 52, 70};
    EDD m = new EDD(p, d);
    m.run();
  }
}
