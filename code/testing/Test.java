import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
/**
Test class runs takes the input data and runs the SPT and EDD algorithms on
these datasets.
The schedules returned are checked to see if they are valid schedules,
as in all on-time jobs are indeed on time.
Check if the schedules returned minimise the same number of tardy jobs
for both algortihms.
**/
public class Test {
  public static void main(String[] args) throws IOException {
    SPT spt;
    EDD edd;
    BufferedReader reader = new BufferedReader(new FileReader("test_input.dat"));
    String line = reader.readLine();
    ArrayList<Integer> processing_times = new ArrayList<Integer>();
    ArrayList<Integer> due_dates = new ArrayList<Integer>();
    int test_num = 0;
    // read the input file data
    while (line != null) {
      if (line.equals("***")) {
        ++test_num;
        System.out.print("----TEST NUMBER ----" + test_num);
        // we have finished retrieving data for this collection of jobs
        // run the algorithm, time it and write the output to a csv file

        // convert collected data to arrays
        int[] p = new int[processing_times.size()];
        int[] d = new int[due_dates.size()];

        Iterator<Integer> iterator_p = processing_times.iterator();
        Iterator<Integer> iterator_d = due_dates.iterator();
        for (int i = 0; i < p.length; ++i) {
            p[i] = iterator_p.next().intValue();
            d[i] = iterator_d.next().intValue();
        }

        // initialise the algorithm
        spt = new SPT(p, d);
        edd = new EDD(p, d);

        Job[] spt_jobs = spt.run();
        Job[] edd_jobs = edd.run();

        int spt_tardy_jobs = 0;
        int edd_tardy_jobs = 0;

        int comp_time = 0;
        // check if SPT-based algorithm schedule is a valid schedule
        for (Job j: spt_jobs) {
          // on-time jobs
          if (j.number > 0) {
            comp_time += j.proc_time;
            if (comp_time > j.due_date) {
              // have an on-time which is actually not on time - algorithm fault
              System.out.println("ERROR INVALID SCHEDULE");
              System.exit(0);
            }
          }
          else {
            ++spt_tardy_jobs;
          }
        }

        comp_time = 0;
        // check if EDD-based algorithm schedule is a valid schedule
        for (Job j: edd_jobs) {
          // on-time jobs
          if (j.number > 0) {
            comp_time += j.proc_time;
            if (comp_time > j.due_date) {
              // have an on-time which is actually not on time - algorithm fault
              System.out.println("ERROR INVALID SCHEDULE");
              System.exit(0);
            }
          }
          else {
            ++edd_tardy_jobs;
          }
        }
        
        // now EDD and SPT -based algorithm should minimise same number of jobs
        if (spt_tardy_jobs != edd_tardy_jobs) {
          System.out.println("ERROR MINIMISED DIFFERENT NUMBER OF TARDY JOBS");
          System.exit(0);
        }
        System.out.println("---TRUE");

        // reinitialise processing times and due dates for the next collection
        // of jobs
        processing_times = new ArrayList<Integer>();
        due_dates = new ArrayList<Integer>();
      }
      else {
        // we have an entry of data of the form p,d
        processing_times.add(Integer.valueOf(line.split(",")[0]));
        due_dates.add(Integer.valueOf(line.split(",")[1]));
      }
      // read the next line
      line = reader.readLine();
    }
    reader.close();
  } //end of main
} //end of Runtime class
