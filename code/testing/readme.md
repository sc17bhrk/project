## Testing

Compares the outputs of the EDD and SPT algorithm schedules.
Checks if they both supply valid schedules and that the number
of late jobs are the same for both schedules.
The process is repeated on multiple different data sets of varying sizes
found in test_input.dat

Run with

      $ javac *.java

      $ java SPT

The processing times and due dates for the jobs in the test data can be changed
by generating a different test_input.dat file, check the data directory.
