/* LeafNode class inherits from Node
Leaf node, l, structure :
  [(u,v)  p  a], where  u is the earliest due date of a job in the block
                        v is the latest due date of a job in the block
                        p is the processing time of all the jobs in the block
                        a is the adjustment value of the leaf node
*/
public class LeafNode extends Node {

    int key;    // to store the value
    int u, v;   //start and end due dates for block
    int p;      // processing time for block

    // contructors
    LeafNode(Job j, Node parent, int adjustment) {
      super(parent, adjustment);
        this.u = this.v = j.due_date;
        this.p = j.proc_time;
    }

    LeafNode(Job j, int adjustment) {
      this(j, null, adjustment);
    }

    LeafNode(Job j, Node parent) {
      this(j, parent, 0);
    }

    LeafNode(Job j) {
      this(j, null, 0);
    } //last contructor

    // setter - sets earliest date
    void setEarliestDate(int u) {
      this.u = u;
    } // setEarliestDate() ends here

    // setter - sets latest date for block
    void setLatestDate(int v) {
      this.v = v;
    } // setLatestDate() ends here

    // updates the processing time
    void addProcTime(int new_p) {
      this.p += new_p;
    } // addProcTime() ends here

    // Print method
    void print() {
        System.out.printf("[(%d,%d) p:%d a:%d] ", u, v, p, this.a);
    } // print() ends here
} // LeafNode ends here
