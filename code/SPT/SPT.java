import java.util.*;
// SPT class implements SPT(Moore's) algorithm
/*
Sort the array of jobs in increasing order of processing time
Cycle through each of the jobs and add it to the TwoThreeTree schedule if we can
otherwise, remove the job from the schedule which has the largest processing time,
this will the current job being considerd, sp skip to next job and attempt insert
schedule jobs in due date order and then tardy jobs in any order
*/
public class SPT {
  private Job[] jobs;
  private TwoThreeTree schedule;

  // constructor
  public SPT(int[] processing_times, int[] due_dates) {
    jobs = new Job[due_dates.length];
    // initialisng jobs with the job number, processing times and due dates
    for (int i = 0; i < jobs.length; ++i) {
      jobs[i] = new Job(i+1, processing_times[i], due_dates[i]);
    } // end for
    schedule = new TwoThreeTree();
  } // constructor ends here

  // run SPT(Moore's) algorithm
  public Job[] run() {
    // reorder jobs in SPT (shortest processing time) order with merge sort
    MergeSort.sort_SPT(jobs);
    // insert artificial jobs
    schedule.insertNewBlock(new Job(0, -1), 0);
    schedule.insertNewBlock(new Job(0, Integer.MAX_VALUE), 0);

    // completion time variables
    int C_prev, C_j, C_next;
    // slack variables of blocks
    int slack_prev, slack_j, slack_next;
    // variables to store the block with its respective starting time
    BlockWithStartTime prevBlock, nextBlock;
    for (Job j : jobs) {
      // assuming there are no previous blocks/ next blocks, we reinitialise
      prevBlock = null;
      nextBlock = null;
      C_prev = 0;
      C_j = 0;
      C_next = 0;
      slack_prev = -1;
      slack_j = -1;
      slack_next = -1;

      // considering where the current job will be inserted, we get the previous block
      prevBlock = schedule.searchPrevBlock(j);
      // calculate completion time and slack for previous block if it exists
      if (prevBlock != null) {
        C_prev = prevBlock.node.p + prevBlock.S;
        slack_prev = prevBlock.node.v - C_prev;
      } // end if prevBlock != null

      // calculate completion time and slack for current job
      C_j = C_prev + j.proc_time;
      slack_j = j.due_date - C_j;

      // we retrive the block after the previous block if it exists
      // otherwise, the nextBlock is the first block in the tree
      nextBlock = schedule.searchNextBlock(j);
      // calculate completion time and slack for next block after job if it exists
      if (nextBlock != null) {
        C_next = nextBlock.node.p + nextBlock.S + j.proc_time;
        slack_next = nextBlock.node.v - C_next;
      } // end if nextBlock != null

      // this job causes later jobs to be tardy, we mark this job as tardy
      if (slack_j < 0 || (slack_next < 0 && nextBlock != null)) {
        // negative job numbers indicate the job is tardy
        j.number = -j.number;
        // skip and consider next job in list
        continue;
      } // end if tardy job

      // we have a valid job - will insert the job
      // we have three slacks - the prev, current and next block slack

      // outcome (i) - new block consisting of single job
      if (slack_prev < slack_j && (slack_j < slack_next || nextBlock == null)) {
        // starting time for new block is the sum of the adjustment value
        // and processing time of previous block if it exists
        int s = 0;
        if (prevBlock != null) {
          s = prevBlock.S + prevBlock.node.p;
        }
        schedule.insertNewBlock(j, s);
      } // end if new block

      // outcome (ii) - job j merged into next block
      else if (slack_j >= slack_next && nextBlock != null) {
        schedule.mergeIntoBlock(j, nextBlock.node);
      } // end if merging job j into block

      // outcome (iii) - previous blocks are merged into blok containing j
      if (slack_prev >= slack_j || (slack_prev >= slack_next && nextBlock != null)) {
        int critical_slack; // is the important slack for the critical job
                            // of the block we are merging into
        // if we haven't merged j into an existing block then insert it
        if (slack_j < slack_next || nextBlock == null) {
          int s = 0; //srating time of job j
          if (prevBlock != null) {
            s = prevBlock.S + prevBlock.node.p;
            schedule.mergeIntoPrevBlock(j, prevBlock.node);
            // get the previous block to this block
            prevBlock = schedule.searchPrevBlock(j);
            // calculate completion time and slack for previous block if it exists
            if (prevBlock != null) {
              C_prev = prevBlock.node.p + prevBlock.S;
              slack_prev = prevBlock.node.v - C_prev;
            } // end if prevBlock != null
            else {
              slack_prev = -1;
            }
          }
          else {
            schedule.insertNewBlock(j, s);
          }

          // we have a new block with job j we are merging into
          // so job j has the critical slack
          critical_slack = slack_j;
        } // end if inserting job
        // we have merged j into nextblock so this slack is the critical slack
        else {
          critical_slack = slack_next;
        } // end else already merged

        // we have definitely inserted job j into the schedule either
        // by merging from outcome (ii) or just now we inserted it
        LeafNode insertedLeaf = schedule.getLastLeafInserted();

        // looping and merging previous blocks with higher slacks into block with j
        while (slack_prev >= critical_slack) {
          schedule.mergeBlocks(prevBlock.node, insertedLeaf);
          // getting the previous block to the block we've just merged together
          prevBlock = schedule.searchPrevBlock(insertedLeaf);
          // update previous block slack
          if (prevBlock != null) {
            C_prev = prevBlock.node.p + prevBlock.S;
            slack_prev = prevBlock.node.v - C_prev;
          }
          else {
            slack_prev = -1;  //doesn't exist so loop will finish
          } //end if else
        } // end while
      } // end if previous blocks are merged
      // -- uncomment next two lines to see tree after each insertion
      // System.out.println();
      // schedule.bfsList();
    } // end for Job j in jobs array

    // print the 2-3 tree representing the schedule of on time jobs -- uncomment
    // schedule.bfsList();

    // sort our jobs in the list in SPT order into EDD order
    // jobs which are late will have negative job numbers
    // -e.g. if job number 7 was tardy, it will now be job number -7
    MergeSort.sort_EDD(jobs);
    // print the on time jobs in order and late jobs
    // ----- uncomment lines below to print schedule
    System.out.println();
    printValidJobs();
    printInvalidJobs();
    System.out.println();
    // -----
    return jobs;
  } // run() ends here

  // prints the jobs in their current state
  public void printAllJobs() {
    for (Job j : jobs) {
      printJob(j);
    }
    System.out.println();
  } // printAllJobs ends here

  // prints details of the job j
  public static void printJob(Job j) {
    String job = j.number + " " + j.proc_time + " " + j.due_date;
    System.out.print(job + ",   ");
  } // printJob() ends here

  // prints all the jobs that are on time - i.e. have positive job number values
  public void printValidJobs() {
    System.out.println("On Time Jobs Schedule Order\n");
    System.out.println(padRight("Number", 10) + padRight("Proc Time", 12) +
                       padRight("Due Date", 12));
    for (Job j : jobs) {
      if (j.number > 0) {
        System.out.println(padRight(String.valueOf(j.number), 10) +
                           padRight(String.valueOf(j.proc_time), 12) +
                           padRight(String.valueOf(j.due_date), 12));
      }
    }
    System.out.println();
  } // printValidJobs() ends here

  // prints all tardy jobs that are on time - i.e. have negative job number values
  public void printInvalidJobs() {
    System.out.println("Late Jobs");
    for (Job j : jobs) {
      if (j.number < 0) {
        System.out.println(padRight(String.valueOf(-j.number), 10) +
        padRight(String.valueOf(j.proc_time), 12) +
        padRight(String.valueOf(j.due_date), 12));
      }
    }
    System.out.println();
  } // printInvalidJobs() ends here

  // pads a string s with spaces to length n
  private static String padRight(String s, int n) {
    return String.format("%-" + n + "s", s);
  } // padRight() ends here

  // main method - used for testing SPT algorithm
  public static void main(String[] args) {
    // test data to run - can modify these values

    // int[] p = {10, 3, 4, 8, 10, 6};
    // int[] d = {15, 6, 9, 23, 20, 30};
    // int[] p = {3, 4, 10, 10, 8, 6};
    // int[] d = {6, 9, 15, 20, 23, 30};
    // int[] p = {1, 2, 3, 4};
    // int[] d = {115, 99, 161, 112};
    // int[] p = {1, 2, 3, 4};
    // int[] d = {115, 117, 161, 112};
    int[] p = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 44, 90};
    int[] d = {115, 99, 161, 112, 170, 155, 60, 85, 160, 112, 52, 138, 171, 99};
    SPT m = new SPT(p, d);
    m.run();
  }
}
