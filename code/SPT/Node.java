// Node class - base class for representing Node in TwoThreeTree
// contains parent and adjustment - both required in TreeNode and LeafNode
public class Node {
  Node parent; // parent of node
  int a;       //adjustment value for node

  // default contructor
  Node(Node node, int adjustment) {
    parent = node;
    a = adjustment;
  } // constructor ends here

  // setter - adjustment
  void setAdjustment(int a) {
    this.a = a;
  } // setAdjustment() ends here

  // adds adjustment
  void addAdjustment(int new_a) {
    this.a += new_a;
  } // addAdjustment() ends here

  // find which child this is of the parent, e.g leftchild returns 0
  // middle returns 1 and tight returns 2
  int whichChild() {
    // the parent could be null if this leafNode is the root
    if (parent == null) {
      return -1;
    }
    if (parent instanceof TreeNode) {
      TreeNode p = (TreeNode) parent;
      // if it is the left child of the parent
      if (this == p.children[0]) {
        return 0;
      }
      // if it is the middle child of the parent
      if (this == p.children[1]) {
        return 1;
      }
      // it must be the right child of the parent
      return 2;
    }
    return -1;
  } // whichChild() ends here
} // Node class definition ends here
