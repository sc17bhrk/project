/*
TreeNode class inherits from Node
Tree node, t, structure :
[(k1,k2)  a], where   k1 is the key storing the value of the latest due date
of the block which is a descendant of the leftmost child of t
k2 is the key storing the value of the latest due date
of the block which is a descendant of the middle child of t
a is the adjustment value of the tree node
*/
public class TreeNode extends Node {
  // Member variables
  int keys[];             // keys for searching
  Node children[];        // references to the 2 or 3 children
  int degree;             // number of children each node has

  // constructors
  TreeNode(Node parent, int adjustment) {
    super(parent, adjustment);
    // Initializing member variables
    keys = new int[2];          // keys[0] = max key in left subtree
    children = new Node[3];     // references to children or leaves
    degree = 0;                 // for printing, overflow, and split operations
  }

  TreeNode(int adjustment) {
    this(null, adjustment);
  }

  TreeNode() {
    this(null, 0);
  } // constructors end here

  // setter - sets the child of the treeNode and sets itself as the parent
  void setChild(int childNum, Node childNode) {
    // we have an error
    if (childNum < 0 || childNum > 2) {
      return;
    }
    this.children[childNum] = childNode;
    childNode.parent = this;
  } //setChild() ends here

  // Print method
  void print() {

    if (degree == 1) {
      System.out.print("[(" + keys[0] + ",-) ");
    } else {
      System.out.print("[(" + keys[0] + "," + keys[1] + ") ");
    }
    System.out.print("a: " + this.a + " deg: " + this.degree + "] ");
  } // print() ends here
} // TreeNode ends here
