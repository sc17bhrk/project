// encapsulates a LeafNode (block) with the starting time S

public  class BlockWithStartTime {
  final LeafNode node;
  final int S;

  public BlockWithStartTime(LeafNode node, int S) {
    this.node = node;
    this.S = S;
  }
}
