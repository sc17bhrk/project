
import java.util.LinkedList;
import java.util.Queue;

/**
*
TwoThreeJava Class heavily adapted from:
  S. Agrawal,
  2-3-Trees,
  2018,
  GitHub repository,
  url: https://github.com/swapnil0399/2-3-Trees,
  commit: 43fe01e253920c9c301e0ed104baaf229b41bd20
*
*/

/*
Originally, the TwoThreeTree stored integers in ascending order
The tree has been altered to store special leaf nodes corresponding to a
block of jobs to be processed in the schedule of on time jobs.
Leaf node, l, structure :
  [(u,v)  p  a], where  u is the earliest due date of a job in the block
                        v is the latest due date of a job in the block
                        p is the processing time of all the jobs in the block
                        a is the adjustment value of the leaf node
Tree node, t, structure :
  [(k1,k2)  a], where   k1 is the key storing the value of the latest due date
                        of the block which is a descendant of the leftmost child of t
                        k2 is the key storing the value of the latest due date
                        of the block which is a descendant of the middle child of t
                        a is the adjustment value of the tree node

Adjustment values keep track of the starting time. The sum of the adjustment
values, S, from the root to a leaf node l, denotes the starting time for the
block of jobs represented by l, to begin. So the completion time of jobs for
block l, is C = S + p, where p is the processing time for the block l.

Example:
                              [(112,161) a: 0]

          [(99,112) a: 0]                          [(115,161) a: 4]

[(99,99) p:2 a:0] [(112,112) p:4 a:2]    [(115,115) p:1 a:2] [(161,161) p:3 a:3]
      job 2              job 4                job 1            job 3

The starting time for job 1 is S = 0 + 4 + 2 = 6,
with completion time C = S + p = 6 + 1 = 7.
So we expect job 3 to have starting time 7, which it does as 0 + 4 + 3 = 7.
*/
public class TwoThreeTree {

  // Private member variables of the class
  private int size;
  private TreeNode root;
  private boolean successfulInsertion;
  private boolean successfulDeletion;
  private boolean split;
  private boolean underflow;
  private boolean first;
  private boolean singleNodeUnderflow;
  private LeafNode insertedLeaf;

  private enum Nodes {
    LEFT, MIDDLE, RIGHT, DUMMY;
  }

  // Default Constructor
  public TwoThreeTree() {
    // Initializing everything to default values
    size = 0;
    root = null;
    successfulInsertion = false;
    successfulDeletion = false;
    underflow = false;
    singleNodeUnderflow = false;
    split = false;
    first = false;
    insertedLeaf = null;
  } // constructor ends here

  // Merge Root: If the root node has to split, it merges it
  // back to a single TreeNode
  private void insertBlock(Job j, int a) {
    // Create an array of Node of size 2
    // to capture the 2 nodes
    Node[] array = new Node[2];
    // Call the method to insert the key
    array = insert(j, root, a);

    // Check if the second value in the array is null or not
    if (array[1] == null) {
      // If it is null, it means there is no need to merge the array
      root = (TreeNode) array[0]; // Simply assign the root to the first value
    } else {
      // We got two values in the array
      // and now we have to merge those into a single node
      // Create a new TreeNode and attach first and second element in the
      // array as a reference in the newly created node
      TreeNode treeRoot = new TreeNode();
      treeRoot.setChild(0, array[0]);
      treeRoot.setChild(1, array[1]);
      updateTree(treeRoot);   // Update the new node
      root = treeRoot;        // Assign root to this node
    } // if else ends here
  } // insertBlock() ends here

  // Insert: Returns an array of nodes(max 2 nodes)
  // after inserting the value
  private Node[] insert(Job j, Node n, int a) {
    // we'll compare the due date of the job to the keys of the treenodes
    int key = j.due_date;
    // Create a new Node array of size 2
    Node array[] = new Node[2];     // [0] old Node, [1] = new Node

    // This array of node stores the result after the recursive insert has returned
    Node catchArray[] = new Node[2];

    TreeNode t = null;  // Initialize t to null

    // If the node is a TreeNode
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    }

    // If the root is null, this means it is the first node
    if (root == null && !first) {
      first = true;   // Switch to make this if false for next recursive call

      // Create a new TreeNode
      TreeNode newNode = new TreeNode();
      t = newNode;
      // Call insert with the given value
      t.children[0] = insert(j, t.children[0], a)[0];
      t.children[0].parent = t;
      updateTree(t);  // Update the tree

      // We will return this array
      // Make first element in the array store the reference of this TreeNode
      array[0] = t;
      array[1] = null;    // Make second element null

    } // If the node on which insert was called is a treeNode and
    // stores references to TreeNodes
    else if (t != null && !(t.children[0] instanceof LeafNode)) {
      // If the key to be inserted is less than the first key
      if (key <= t.keys[0]) {
        // Recursively call insert on the left children node
        catchArray = insert(j, t.children[0], a - t.a);
        // Store the reference returned by the call
        t.children[0] = catchArray[0];

        // If split has occurred
        if (split) {
          // If the degree of current node is less than or equal to 2
          if (t.degree <= 2) {
            // Make split false we will handle it right here
            split = false;
            // Attach new nodes and update the tree
            t.children[2] = t.children[1];
            t.children[1] = catchArray[1];
            updateTree(t);
            array[0] = t;
            array[1] = null;
          } else if (t.degree > 2) {
            // In this case, we are going to pass the two nodes back
            // up the chain - new treenode has the same adjustment value
            TreeNode newNode = new TreeNode(t.parent, t.a);
            newNode.setChild(0, t.children[1]);
            newNode.setChild(1, t.children[2]);
            updateTree(newNode);
            t.children[1] = catchArray[1];
            t.children[2] = null;
            updateTree(t);
            array[0] = t;
            array[1] = newNode;
          }
        } else {
          // If there was no split simply update the tree and
          // pass the reference up the tree
          updateTree(t);
          array[0] = t;
          array[1] = null;
        } // if else for split ends here
      } // If for key <= t.keys[0] ends here
      // If the key to be inserted is greater than the first and less than the second
      // or third is null
      else if (key > t.keys[0] && (t.children[2] == null || key <= t.keys[1])) {
        // Recursively call insert on the middle children node
        catchArray = insert(j, t.children[1], a - t.a);
        // Store the reference returned by the call
        t.children[1] = catchArray[0];

        // If split has occurred
        if (split) {
          // If the degree of current node is less than or equal to 2
          if (t.degree <= 2) {
            // Make split false we will handle it right here
            split = false;
            // Attach new nodes and update the tree
            t.children[2] = catchArray[1];
            updateTree(t);
            array[0] = t;
            array[1] = null;
          } else if (t.degree > 2) {
            // In this case, we are gonna pass the two nodes back
            // up the chain hoping it will be handled there
            TreeNode newNode = new TreeNode(t.parent, t.a);
            newNode.setChild(0, catchArray[1]);
            newNode.setChild(1, t.children[2]);
            updateTree(newNode);
            t.children[2] = null;
            updateTree(t);
            array[0] = t;
            array[1] = newNode;
          }
        } else {
          // If there was no split simply update the tree and
          // pass the reference up the tree
          updateTree(t);
          array[0] = t;
          array[1] = null;
        } // if else for split ends here
      } // If for key > t.keys[0] ends here
      // If the key to be inserted is greater than second key
      else if (key > t.keys[1]) {
        // Recursively call insert on the right children node
        catchArray = insert(j, t.children[2], a - t.a);
        // Store the reference returned by the call
        t.children[2] = catchArray[0];

        // If split has occurred
        if (split) {
          if (t.degree > 2) {
            // In this case, we are gonna pass the two nodes back
            // up the chain hoping it will be handled there
            TreeNode newNode = new TreeNode(t.parent, t.a);
            newNode.setChild(0, catchArray[0]);
            newNode.setChild(1, catchArray[1]);
            updateTree(newNode);
            t.children[2] = null;
            updateTree(t);
            array[0] = t;
            array[1] = newNode;

          }
        } else {
          // If there was no split simply update the tree and
          // pass the reference up the tree
          updateTree(t);
          array[0] = t;
          array[1] = null;
        } // if else for split ends here
      } // If for key > t.keys[1] ends here
    } // If the node on which insert was called is a treeNode and
    // stores references to LeafNodes
    else if (t != null && t.children[0] instanceof LeafNode) {
      // Just get the references of all the children
      LeafNode l1 = null, l2 = null, l3 = null;
      if (t.children[0] != null && t.children[0] instanceof LeafNode) {
        l1 = (LeafNode) t.children[0];
      }
      if (t.children[1] != null && t.children[1] instanceof LeafNode) {
        l2 = (LeafNode) t.children[1];
      }
      if (t.children[2] != null && t.children[2] instanceof LeafNode) {
        l3 = (LeafNode) t.children[2];
      }
      // we will be adding a new leaf node here
      LeafNode leaf = new LeafNode(j, t, a - t.a);
      insertedLeaf = leaf;

      // If the degree of current TreeNode is less than or equal to 2
      if (t.degree <= 2) {
        // Simply find the place and insert the value
        if (t.degree == 1 && key > l1.v) {
          t.children[1] = leaf;
        } else if (t.degree == 1 && key < l1.v) {
          t.children[1] = l1;
          t.children[0] = leaf;
        } else if (t.degree == 2 && key < l1.v) {
          t.children[2] = l2;
          t.children[1] = l1;
          t.children[0] = leaf;
        } else if (t.degree == 2 && key < l2.v && key > l1.v) {
          t.children[2] = l2;
          t.children[1] = leaf;
        } else if (t.degree == 2) {
          t.children[2] = leaf;
        }

        // Update the tree and pass the array back
        updateTree(t);
        array[0] = t;
        array[1] = null;
      } // If the degree is greater than 2
      else if (t.degree > 2) {
        TreeNode newNode = new TreeNode(t.parent, t.a);
        // Now we have to split the given treeNode
        split = true;
        // After split both nodes will have two children

        // Now separate the leaf nodes and attach it to the TreeNode and
        // pass the references of both the TreeNodes back
        if (key < l1.v) {
          // If the new node to be inserted is less than the first key
          // Store the new key node and the first key in one array and
          // the remaining in the second array
          // Pass both the array back
          t.children[0] = leaf;
          t.children[1] = l1;
          t.children[2] = null;
          updateTree(t);
          newNode.setChild(0, l2);
          newNode.setChild(1, l3);
          updateTree(newNode);
          array[0] = t;
          array[1] = newNode;
        } else if (key >= l1.v && key < l2.v) {
          // If the new node to be inserted is between the first key and the second key
          // Store the new key node and the first key in one array and
          // the remaining in the second array
          // Pass both the array back
          t.children[1] = leaf;
          t.children[2] = null;
          updateTree(t);
          newNode.setChild(0, l2);
          newNode.setChild(1, l3);
          updateTree(newNode);
          array[0] = t;
          array[1] = newNode;
        } else if (key >= l2.v && key < l3.v) {
          // If the new node to be inserted is between the third key and the second key
          // Store the new key node and the third key in one array and
          // the remaining in the second array
          // Pass both the array back
          t.children[2] = null;
          updateTree(t);
          newNode.setChild(0, leaf);
          newNode.setChild(1, l3);
          updateTree(newNode);
          array[0] = t;
          array[1] = newNode;
        } else if (key >= l3.v) {
          // If the new node to be inserted is greater than the third key
          // Store the new key node and the third key in one array and
          // the remaining in the second array
          // Pass both the array back
          t.children[2] = null;
          updateTree(t);
          newNode.setChild(0, l3);
          newNode.setChild(1, leaf);
          updateTree(newNode);
          array[0] = t;
          array[1] = newNode;
        } // if else ends here
      } // outer else if ends here
      // If we have made it till here we have successfully inserted the value
      successfulInsertion = true;
    } else if (n == null) {
      // If the node was null it means there is no leafNode (so we have no tree)
      // Pass the reference to the new leafNode back
      successfulInsertion = true;
      LeafNode leaf = new LeafNode(j, t, a);
      insertedLeaf = leaf;
      array[0] = leaf;
      array[1] = null;
      return array;
    }
    // Return the array
    return array;
  } // recursive insert ends here

  // This recursive method removes the given key from the tree
  private Node remove(LeafNode l, Node n) {
    // the last due date of the block in LeafNode l will be the key compared
    int key = l.v;
    // If the given node is the instance of a TreeNode
    TreeNode t = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    }

    // If the node is null return null else continue
    if (n == null) {
      return null;
    }

    // If the children is the instance of TreeNode
    // This means we haven't reached the node where we want to perform delete
    if (t != null && t.children[0] instanceof TreeNode) {
      // If the given key to be deleted is less than the first key
      if (key <= t.keys[0]) {
        // Perform deletion on first child with the same key
        t.children[0] = remove(l, t.children[0]);

        // If after deletion there is a degree 1 node in the tree
        if (singleNodeUnderflow) {
          // Get the reference of both of the child nodes
          TreeNode child = (TreeNode) t.children[0];
          TreeNode rightChild = (TreeNode) t.children[1];

          // If the right child has a degree of 2
          // Attach the child to the rightchild and update the tree
          if (rightChild.degree == 2) {
            rightChild.children[2] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[0];
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - rightChild.a);
            rightChild.setChild(0, child);
            updateTree(rightChild);
            t.children[0] = rightChild;
            t.children[1] = t.children[2];
            t.children[2] = null;

            // If the degree of the current TreeNode is 2
            // then it will also underflow after this
            if (t.degree == 2) {
              singleNodeUnderflow = true;
              updateTree(t);
              t = (TreeNode) t.children[0];
            } else {
              singleNodeUnderflow = false;
            }
          } // If the right child has a degree of 3
          // Borrow one child from rightchild and attach it to
          // newly created tree node which has 2 children
          // one a child from rightchild and the other child of
          // the Tree node which underflew
          else if (rightChild.degree == 3) {
            TreeNode newNode = new TreeNode(t, rightChild.a);
            // modify adjustment for the lone child being moved
            child.addAdjustment(child.parent.a - rightChild.a);
            newNode.setChild(0, child);
            newNode.setChild(1, rightChild.children[0]);
            t.setChild(0, newNode);
            updateTree(newNode);
            rightChild.children[0] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[2];
            rightChild.children[2] = null;
            updateTree(rightChild);
            singleNodeUnderflow = false;
          }
        } // if for singleNodeUnderflow ends here
        // Else if there was an underflow
        else if (underflow) {
          // We will handle it here
          underflow = false;
          // Get the references of children nodes
          TreeNode child = (TreeNode) t.children[0];
          TreeNode rightChild = (TreeNode) t.children[1];

          // If the degree of rightchild is 3 then borrow one
          // child from the rightchild and attach it to the child
          if (rightChild.degree == 3) {
            Node reference = rightChild.children[0];
            rightChild.children[0] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[2];
            rightChild.children[2] = null;
            updateTree(rightChild);
            child.setChild(1, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(rightChild.a - child.a);
            updateTree(child);
          } // If the degree of rightchild is 2 then merge child
          // and rightchild into one node
          else if (rightChild.degree == 2) {
            Node reference = child.children[0];
            rightChild.children[2] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[0];
            rightChild.setChild(0, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(child.a - rightChild.a);
            updateTree(rightChild);
            t.children[0] = rightChild;

            // Now if the degree of the treeNode was 3
            // we are good this won't underflow after merge
            if (t.degree == 3) {
              t.children[1] = t.children[2];
              t.children[2] = null;
            } // But if it's less than 3, it will certainly underflow
            // In this case we are gonna pass the problem up the tree
            else {
              Node ref = t.children[0];
              t = (TreeNode) ref;
              singleNodeUnderflow = true;
            } // nested inner if else ends here
          } // inner if else ends here
        } // outer if else ends here
        updateTree(t); // update the tree
      } // If the given key to be deleted is in between first and second key
      else if (key > t.keys[0] && (t.children[2] == null || key <= t.keys[1])) {
        // Perform deletion on second child with the same key
        t.children[1] = remove(l, t.children[1]);

        // If after deletion there is a degree 1 node in the tree
        if (singleNodeUnderflow) {
          // Get references of all the child nodes
          TreeNode leftChild = (TreeNode) t.children[0];
          TreeNode child = (TreeNode) t.children[1];
          TreeNode rightChild = (TreeNode) t.children[2]; // may be null

          // If the left child has a degree of 2
          // Attach the child to the leftchild and update the tree
          if (leftChild.degree == 2) {
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - leftChild.a);
            leftChild.setChild(2, child);

            t.children[1] = rightChild;
            t.children[2] = null;
            updateTree(leftChild);

            // If the degree of the current TreeNode is 2
            // then it will also underflow after this merge
            if (t.degree == 2) {
              singleNodeUnderflow = true;
              updateTree(t);
              t = (TreeNode) t.children[0];
            } else {
              singleNodeUnderflow = false;
            }
          } // If the right child has a degree of 2
          // Attach the child to the rightchild and update the tree
          else if (rightChild != null && rightChild.degree == 2) {
            rightChild.children[2] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[0];
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - rightChild.a);
            rightChild.setChild(0, child);
            updateTree(rightChild);

            t.children[1] = rightChild;
            t.children[2] = null;
            // clearly t originally had degree 3, so we have no underflow
            singleNodeUnderflow = false;

          } // If the degree of leftchild is 3 then borrow one
          // child from the leftchild and attach it to the newly
          // created node which has a child of leftchild and the
          // child which underflew
          else if (leftChild.degree == 3) {
            TreeNode newNode = new TreeNode(t, leftChild.a);
            newNode.setChild(0, leftChild.children[2]);
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - leftChild.a);
            newNode.setChild(1, child);
            t.setChild(1, newNode);
            leftChild.children[2] = null;
            updateTree(newNode);
            updateTree(leftChild);
            singleNodeUnderflow = false;
          } // If the degree of rightchild is 3 then borrow one
          // child from the rightchild and attach it to the newly
          // created node which has a child of rightchild and the
          // child which underflew
          else if (rightChild != null && rightChild.degree == 3) {
            TreeNode newNode = new TreeNode(t, rightChild.a);
            newNode.setChild(0, child);
            // modify the adjustment for the child that has been moved
            child.addAdjustment(t.a - rightChild.a);
            newNode.setChild(1, rightChild.children[0]);

            rightChild.children[0] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[2];
            rightChild.children[2] = null;

            t.setChild(1, newNode);
            updateTree(newNode);
            updateTree(rightChild);
            singleNodeUnderflow = false;
          }
        } // Else if there was an underflow
        else if (underflow) {
          // We will handle it here
          underflow = false;

          // Get the references of children nodes
          TreeNode leftChild = (TreeNode) t.children[0];
          TreeNode child = (TreeNode) t.children[1];
          TreeNode rightChild = (TreeNode) t.children[2];

          // If the degree of leftchild is 3 then borrow one
          // child from the leftchild and attach it to the child
          if (leftChild.degree == 3) {
            Node reference = leftChild.children[2];
            leftChild.children[2] = null;
            child.children[1] = child.children[0];
            child.setChild(0, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(leftChild.a - child.a);
            updateTree(leftChild);
            updateTree(child);
          } // If the degree of rightchild is 3 then borrow one
          // child from the rightchild and attach it to the child
          else if (rightChild != null && rightChild.degree == 3) {
            Node reference = rightChild.children[0];
            rightChild.children[0] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[2];
            rightChild.children[2] = null;
            updateTree(rightChild);

            child.setChild(1, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(rightChild.a - child.a);
            updateTree(child);
          } // If the degree of leftchild is 2 then merge child
          // and rightchild into one node
          else if (leftChild.degree == 2) {
            Node reference = child.children[0];
            leftChild.setChild(2, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(child.a - leftChild.a);
            updateTree(leftChild);
            t.children[1] = null;

            // Now if the degree of the treeNode is 3
            // we are good this won't underflow after merge
            if (t.degree == 3) {
              t.children[1] = t.children[2];
              t.children[2] = null;
            } // But if it's less than 3, it will certainly underflow
            // In this case we are gonna pass the problem up the tree
            else {
              singleNodeUnderflow = true;
              updateTree(t);
              t = (TreeNode) t.children[0];
            }
          } // If the degree of rightchild is 2 then merge child
          // and rightchild into one node
          else if (rightChild != null && rightChild.degree == 2) {
            Node reference = child.children[0];
            rightChild.children[2] = rightChild.children[1];
            rightChild.children[1] = rightChild.children[0];
            rightChild.setChild(0, reference);
            // modify the adjustment for the child that has been moved
            reference.addAdjustment(child.a - rightChild.a);
            updateTree(rightChild);

            t.children[1] = rightChild;
            t.children[2] = null;
            singleNodeUnderflow = false;
          } // inner if else ends here
        } // outer if else ends here
        updateTree(t); // update the tree
      } // If the given key to be deleted is greater than the second key
      else if (key > t.keys[1]) {
        // Perform deletion on third child with the same key
        t.children[2] = remove(l, t.children[2]);

        // If after deletion there is a degree 1 node in the tree
        if (singleNodeUnderflow) {
          // System.out.println("SINGLE NODE UNDERFLOW");
          // Get the reference of both of the child nodes
          TreeNode child = (TreeNode) t.children[2];
          TreeNode leftChild = (TreeNode) t.children[1];

          // If the left child has a degree of 2
          // Attach the child to the leftchild and update the tree
          if (leftChild.degree == 2) {
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - leftChild.a);
            leftChild.setChild(2, child);

            t.children[2] = null;
            updateTree(leftChild);
          } // If the left child has a degree of 3
          // Borrow one child from leftchild and attach it to
          // newly created tree node which has 2 children
          // one a child from leftchild and the other child of
          // the Tree node which underflew
          else if (leftChild.degree == 3) {
            TreeNode newNode = new TreeNode(t, leftChild.a);
            // modify the adjustment for the child that has been moved
            child.addAdjustment(child.parent.a - leftChild.a);
            newNode.setChild(0, leftChild.children[2]);
            newNode.setChild(1, child);
            t.setChild(2, newNode);
            leftChild.children[2] = null;
            updateTree(newNode);
            updateTree(leftChild);
          }
          // We have handled singleNodeUnderflow here
          singleNodeUnderflow = false;
        } // Else if there was an underflow
        else if (underflow) {
          // We will handle it here
          underflow = false;
          // Get the references of children nodes
          TreeNode leftChild = (TreeNode) t.children[1];
          TreeNode child = (TreeNode) t.children[2];

          // If the degree of leftchild is 3 then borrow one
          // child from the leftchild and attach it to the child
          if (leftChild.degree == 3) {
            Node reference = leftChild.children[2];
            leftChild.children[2] = null;
            child.children[1] = child.children[0];
            child.setChild(0, reference);
            reference.addAdjustment(leftChild.a - child.a);
            updateTree(leftChild);
            updateTree(child);
          } // If the degree of leftchild is 2 then merge child
          // and leftchild into one node
          else if (leftChild.degree == 2) {
            Node reference = child.children[0];
            leftChild.setChild(2, reference);
            reference.addAdjustment(child.a - leftChild.a);
            updateTree(leftChild);
            t.children[2] = null;
          } // inner if else ends here
        } // outer if else ends here
        updateTree(t); // update the tree
      } // outer outer if else ends here
    } // If the tree node has its children as instance of leaf node
    else if (t != null && t.children[0] instanceof LeafNode) {

      // Get the reference of all the leaves in leaf node objects
      LeafNode l1 = null, l2 = null, l3 = null;
      if (t.children[0] != null && t.children[0] instanceof LeafNode) {
        l1 = (LeafNode) t.children[0];
      }
      if (t.children[1] != null && t.children[1] instanceof LeafNode) {
        l2 = (LeafNode) t.children[1];
      }
      if (t.children[2] != null && t.children[2] instanceof LeafNode) {
        l3 = (LeafNode) t.children[2];
      }

      // If all the three leaves are present in the tree node
      if (t.degree == 3) {
        // Find which block to be deleted and adjust the leaves to maintain the property of the tree
        if (l == l1) {
          t.children[0] = l2;
          t.children[1] = l3;
          t.children[2] = null;
        } else if (l == l2) {
          t.children[1] = l3;
          t.children[2] = null;
        } else if (l == l3) {
          t.children[2] = null;
        }
        // Update tree after deletion
        updateTree(t);
      } // If there are just two leaves present in the tree node
      else if (t.degree == 2) {
        // The given node is going to underflow after deletion
        underflow = true;

        // Find the leaf node to be deleted and adjust the leaves to maintain the property of the tree
        if (l1 == l) {
          t.children[0] = l2;
          t.children[1] = null;
        } else if (l2 == l) {
          t.children[1] = null;
        }
      } // If there is just one leaf node in the tree node
      else if (t.degree == 1) {
        // Just delete the leaf node if it matches the leaf node to be deleted
        if (l1 == l) {
          t.children[0] = null;
        }
      } // inner if else ends here

      // The deletion is successful
      successfulDeletion = true;
    } // outer if else ends here
    updateTree(t);
    return t;
  } // remove() ends here

  // This mehtod updates the info stored in the TreeNodes
  private void updateTree(TreeNode t) {

    // If t is not null
    if (t != null) {
      // Check how many children does the current node have
      if (t.children[2] != null && t.children[1] != null && t.children[0] != null) {
        // If it has three children
        t.degree = 3;
        // Get values for both the keys
        t.keys[0] = getValueForKey(t, Nodes.LEFT);
        t.keys[1] = getValueForKey(t, Nodes.RIGHT);
      } else if (t.children[1] != null && t.children[0] != null) {
        // If it has two children
        t.degree = 2;
        // Get value for both keys
        t.keys[0] = getValueForKey(t, Nodes.LEFT);
        t.keys[1] = getValueForKey(t, Nodes.RIGHT);
      } else if (t.children[0] != null) {
        // If it has a single child
        t.degree = 1;
        // get value for first key
        t.keys[0] = getValueForKey(t, Nodes.LEFT);
        t.keys[1] = 0;
      } // if else ends here
    } // outer if ends here
  } // updateTree() ends here

  // This method returns the value for the keys of the TreeNode
  private int getValueForKey(Node n, Nodes whichVal) {
    // Initialize key to -1
    int key = -1;

    // To determine the type of the node
    TreeNode t = null;
    LeafNode l = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    } else {
      l = (LeafNode) n;
    }

    // If it is a leaf node just return the key - the larger due date
    if (l != null) {
      return l.v;
    }
    // If it is a TreeNode
    if (t != null) {
      // Check for which key we want to get value
      if (null != whichVal) {
        switch (whichVal) {
          // If it is the left key
          case LEFT:
          // Go in the left node and then keep going towards
          // last children using recursive calls
          key = getValueForKey(t.children[0], Nodes.DUMMY);
          break;
          // If it is the right key
          case RIGHT:
          // Go in the middle node and then keep going towards
          // last children using recursive calls
          key = getValueForKey(t.children[1], Nodes.DUMMY);
          break;
          // This case executes after we have determined which node
          // we want to go for
          case DUMMY:
          // Way to last children (either middle or right) of the treeNode
          if (t.children[2] != null) {
            key = getValueForKey(t.children[2], Nodes.DUMMY);
          }
          else {
            key = getValueForKey(t.children[1], Nodes.DUMMY);
          }
          break;
          default:
          break;
        } // switch ends here
      } // inner if ends here
    } // outer if ends here

    // Return value for the key
    return key;
  } // getValueForKey() ends here

  // This method updates the adjustment values for all the
  // right siblings of the given leafNode and the right siblings of its
  // parents and so on
  private void updateAdjustment(Node n, int a) {
    // if n is the root
    if (n.parent == null) {
      return;
    }
    // we know the parent is a TreeNode by construction
    TreeNode t = (TreeNode) n.parent;
    int childNum = n.whichChild();
    // add adjustment to middle child and rightmost child if it exists
    if (childNum == 0 && t.degree > 1) {
      t.children[1].addAdjustment(a);
      if (t.children[2] != null) {
        t.children[2].addAdjustment(a);
      }
    }
    // add adjustment to rightmost child if it exists
    else if (childNum == 1) {
      if (t.children[2] != null) {
        t.children[2].addAdjustment(a);
      }
    }
    // repeat recursively for parent
    updateAdjustment(t, a);
  } // updateAdjustment() ends here

  // This method searches the given leaf node in the tree
  private LeafNode search(LeafNode leaf, Node n) {
    // Let found to be null
    LeafNode found = null;
    int key = leaf.v;

    // Determine the type of node
    TreeNode t = null;
    LeafNode l = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    } else {
      l = (LeafNode) n;
    }

    // If it is a TreeNode
    if (t != null) {
      // If it has a degree of 1
      if (t.degree == 1) {
        // Continue search in left node
        found = search(leaf, t.children[0]);
      } // If it has a degree of 2 and the value of key is less than 1st key
      else if (t.degree == 2 && key <= t.keys[0]) {
        // Continue search in left node
        found = search(leaf, t.children[0]);
      } // If it has a degree of 2 and the value of key is greater than 1st key
      else if (t.degree == 2 && key > t.keys[0]) {
        // Continue search in middle node
        found = search(leaf, t.children[1]);
      } // If it has a degree of 3 and the value to be searched for is less than 1st key
      else if (t.degree == 3 && key <= t.keys[0]) {
        // Continue search in left node
        found = search(leaf, t.children[0]);
      } // If it has a degree of 3 and the value to be searched for is in between 1st and 2nd key
      else if (t.degree == 3 && key > t.keys[0] && key <= t.keys[1]) {
        // Continue search in middle node
        found = search(leaf, t.children[1]);
      } // If it has a degree of 3 and the value to be searched for is greater than 2nd key
      else if (t.degree == 3 && key > t.keys[1]) {
        // Continue search in right node
        found = search(leaf, t.children[2]);
      } // inner if else ends here
    } // If it is a leaf node and value matches the one we are searching
    else if (l != null && leaf == l) {
      return l;
    } // outer if else ends here

    return found;
  } // search() ends here

  // This method searches for block in the tree
  // starting time for node n is denoted by S
  // everytime we go further down in the tree we add the adjustment values
  // from nodes on the way to get the starting time S
  private BlockWithStartTime searchPrevBlock(Job j, Node n, int S) {
    // let the block be null and the key be the job's due date
    int key = j.due_date;
    BlockWithStartTime prevBlock = null;

    // Determine the type of node
    TreeNode t = null;
    LeafNode l = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    } else {
      l = (LeafNode) n;
    }

    // If it is a TreeNode
    if (t != null) {
      // if it's child is a leafnode
      if (t.children[0] instanceof LeafNode) {
        // Get the reference of all the leaves in leaf node objects
        LeafNode l1 = null, l2 = null, l3 = null;
        // we find and return the leaf with the largest due date
        // that is still smaller than our current job's due date
        if (t.children[2] != null && t.children[2] instanceof LeafNode) {
          l3 = (LeafNode) t.children[2];
          if (l3.v < key) {
            return new BlockWithStartTime(l3, S + t.a +l3.a);
          }
        }
        if (t.children[1] != null && t.children[1] instanceof LeafNode) {
          l2 = (LeafNode) t.children[1];
          if (l2.v < key) {
            return new BlockWithStartTime(l2, S + t.a +l2.a);
          }
        }
        if (t.children[0] != null && t.children[0] instanceof LeafNode) {
          l1 = (LeafNode) t.children[0];
          if (l1.v < key) {
            return new BlockWithStartTime(l1, S + t.a +l1.a);
          }
        }
        // this current tree node, t, doesn't have the desired leaf node
        // the desired leaf node is the rightmost child of the left sibling of t
        // if it exists, we return and pass the problem up the tree
        return null;
      }
      // we add adjustment from node t to get the starting time S to next child
      S += t.a;
      // If it has a degree of 1
      if (t.degree == 1) {
        // Continue search in left node
        prevBlock = searchPrevBlock(j, t.children[0], S);
      } // If it has a degree of 2 and the value of key is less than 1st key
      else if (t.degree == 2) {
        if (key <= t.keys[0]) {
          // Continue search in left node
          prevBlock = searchPrevBlock(j, t.children[0], S);
        } // If it has a degree of 2 and the value of key is greater than 1st key
        else if (key > t.keys[0]) {
          // Continue search in middle node and then left if not found
          prevBlock = searchPrevBlock(j, t.children[1], S);
          if (prevBlock == null) {
            prevBlock = searchPrevBlock(j, t.children[0], S);
          }
        }
      } // If it has a degree of 2 and the value of key is greater than 1st key
      else if (t.degree == 3) {
        if (key <= t.keys[0]) {
          // Continue search in left node
          prevBlock = searchPrevBlock(j, t.children[0], S);
        } // If it has a degree of 3 and the value to be searched for is in between 1st and 2nd key
        else if (key > t.keys[0] && key <= t.keys[1]) {
          // Continue search in middle node and then leftnode if not found
          prevBlock = searchPrevBlock(j, t.children[1], S);
          if (prevBlock == null) {
            prevBlock = searchPrevBlock(j, t.children[0], S);
          }
        } // If it has a degree of 3 and the value to be searched for is greater than 2nd key
        else if (key > t.keys[1]) {
          // Continue search in right node, then middle if not found
          prevBlock = searchPrevBlock(j, t.children[2], S);
          if (prevBlock == null) {
            // if the desired leaf wasn't found, it will definitely be a
            // descendant of the middle child
            prevBlock = searchPrevBlock(j, t.children[1], S);
          }
        }
      } // inner if else ends here
    } // If it is a leaf node and value matches the one we are searching
    return prevBlock;
  } // searchPrevBlock() ends here

  // This method searches for block in the tree after the previous block
  private BlockWithStartTime searchNextBlock(Job j, Node n, int S) {
    // retrive the prvious block
    BlockWithStartTime prevBlock = searchPrevBlock(j, n, S);
    // if there is no block with due date before current job j
    // the next block is the first block to be processed
    if (prevBlock == null) {
      return getEarliestBlock(root, 0);
    }
    // we do have a previous block, the next block is the right sibling
    return getRightSibling(prevBlock.node, prevBlock.S);
  } //searchNextBlock() ends here

  // given a leafNode l, we find the right sibling of n, n' if it exists and
  // return n' with the new starting time S'
  private BlockWithStartTime getRightSibling(Node l, int S) {
    // leafnode is the root, there is no right sibling as l is the only leaf
    if (l.parent == null) {
      return null;
    }

    // we know the parent of l is a TreeNode by construction
    TreeNode t = (TreeNode) l.parent;
    // used to keep track of the alternating parent and child nodes
    TreeNode childOfT = new TreeNode();
    int childNum = l.whichChild();
    // initialising adjustment for childOfT to be equal to l
    childOfT.a = l.a;

    // want to find right sibling of this leafnode
    // check immediate right siblings if there are any
    // and if there aren't check children of parent's right siblings
    while (t != null) {
      // altering adjustment for starting value
      S -= childOfT.a;
      // checking right sibling of childOfT
      if (childNum <= 1) {
        Node child = t.children[childNum + 1];
        if (child != null) {
          // we have the desired leaf we want,
          if (child instanceof LeafNode) {
            S += child.a;
            return new BlockWithStartTime((LeafNode)child, S);
          }
          // child is a treeNode, we explore the leftmost children of child
          while (child instanceof TreeNode) {
            S += child.a;
            child = ((TreeNode) child).children[0];
          }
          // child is now the leafNode (block) we want
          S += child.a;
          return new BlockWithStartTime((LeafNode) child, S);
        }
      }
      // going up the tree
      childOfT = t;
      t = (TreeNode) childOfT.parent;
      childNum = childOfT.whichChild();
    }

    // the start leafNode l is already the rightmost child
    return null;
  } // getRightSibling() ends here

  // gets the earliest block in the tree - the leftmost leaf with starting time S
  private BlockWithStartTime getEarliestBlock(Node n, int S) {
    // root is null - there are no blocks
    if (n == null) {
      return null;
    }
    // found our desired leftmost leafnode
    if (n instanceof LeafNode) {
      return new BlockWithStartTime((LeafNode) n, S + n.a);
    }
    // otherwise n is a TreeNode
    TreeNode t = (TreeNode) n;
    // traverse left side of tree
    return getEarliestBlock(t.children[0], S + t.a);
  } // getEarliestBlock() ends here

  // This method prints the keys stored in the leafNodes in order
  private void keyOrderList(Node n) {

    // Determine the type of node
    TreeNode t = null;
    LeafNode l = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    } else {
      l = (LeafNode) n;
    }

    // If it is a TreeNode
    if (t != null) {
      // If the first child is not null
      if (t.children[0] != null) {
        // Continue recursive call on 1st child
        keyOrderList(t.children[0]);
      }

      // If the second child is not null
      if (t.children[1] != null) {
        // Continue recursive call on 2nd child
        keyOrderList(t.children[1]);
      }

      // If the third child is not null
      if (t.children[2] != null) {
        // Continue recursive call on 3rd child
        keyOrderList(t.children[2]);
      }
    } // If it is LeafNode
    else if (l != null) {
      // Just print the given leaf
      l.print();
    } // outer if else ends here
  } // keyOrderList() ends here

  // This method just prints the keys on each level
  private void bfsList(Node n) {
    // Make two new queues to store each alternate levels in the queue
    Queue<Node> queueOne = new LinkedList<>();
    Queue<Node> queueTwo = new LinkedList<>();

    // If the first node is null quit else continue
    if (n == null) {
      return;
    }

    // Add the first node to the first queue
    queueOne.add(n);

    // Just declare two Node variables to hold the nodes
    Node first = null;
    TreeNode t = null;
    // used to calulate spacing for printing between tree nodes at different levels
    int height = height();
    int size = this.size/2;

    // Loop until both the queues are non-empty
    while (!queueOne.isEmpty() || !queueTwo.isEmpty()) {

      // If the first queue is not empty
      int count = 0;
      while (!queueOne.isEmpty()) {
        // Take the first element from the queue
        first = queueOne.poll();
        // If it is a tree node print the node
        if (first instanceof TreeNode) {
          t = (TreeNode) first;
          // want more space in between leaf nodes
          if (count > 0) {
            size = this.size;
          } else {
            size = this.size/2;
          }
          for (int i = 0; i <= size * Math.pow(2.78, height);  ++i) {
            System.out.print(" ");
          }
          t.print();
          ++count;
        }

        // Now add all the children on the next level to second queue
        if (t.children[0] != null && !(t.children[0] instanceof LeafNode)) {
          queueTwo.add(t.children[0]);
        }
        if (t.children[1] != null && !(t.children[1] instanceof LeafNode)) {
          queueTwo.add(t.children[1]);
        }
        if (t.children[2] != null && !(t.children[2] instanceof LeafNode)) {
          queueTwo.add(t.children[2]);
        }

      } // first inner loop ends here
      // Leave a blank line after printing a level
      if (!queueOne.isEmpty() || !queueTwo.isEmpty()) {
        System.out.println();
        System.out.println();
      }
      --height;
      count = 0;

      // If the second queue is not empty
      while (!queueTwo.isEmpty()) {
        // Take the first element from the queue
        first = queueTwo.poll();

        // If it is a tree node print the node
        if (first instanceof TreeNode) {
          t = (TreeNode) first;
          // want more space in between leaf nodes
          if (count > 0) {
            size = this.size;
          } else {
            size = this.size/2;
          }
          for (int i = 0; i <= size * Math.pow(2.78, height);  ++i) {
            System.out.print(" ");
          }
          t.print();
          ++count;
        }

        // Now add all the children on the next level to first queue
        if (t.children[0] != null && !(t.children[0] instanceof LeafNode)) {
          queueOne.add(t.children[0]);
        }
        if (t.children[1] != null && !(t.children[1] instanceof LeafNode)) {
          queueOne.add(t.children[1]);
        }
        if (t.children[2] != null && !(t.children[2] instanceof LeafNode)) {
          queueOne.add(t.children[2]);
        }
        --height;
        count = 0;

      } // second inner loop ends here
      // Leave a blank line after printing a level
      if (!queueOne.isEmpty() || !queueTwo.isEmpty()) {
        System.out.println();
        System.out.println();
      }
    } // outer loop ends here

    // Print blank lines before and after printing the keys
    System.out.println();
    System.out.println();
    keyOrderList(root);
    System.out.println();
    System.out.println();
    System.out.print("****************************************");
    System.out.println("**************************************");
    System.out.println();
  } // bfsList() ends here

  // This method determines the height of the tree
  private int height(Node n) {

    // Determine the type of the node
    TreeNode t = null;
    LeafNode l = null;
    if (n instanceof TreeNode) {
      t = (TreeNode) n;
    } else {
      l = (LeafNode) n;
    }

    // If the node is a Tree node and is not null
    if (t != null) {
      // Calculate height of left child and add 1
      return 1 + height(t.children[0]);
    }

    return 0;
  } // height() ends here

  // This is a public method to call search
  public boolean search(LeafNode l) {
    // Just return the search result
    return search(l, root) == l;
  } // search() ends here

  // This method returns the last leafNode inserted into the tree
  public LeafNode getLastLeafInserted() {
    return insertedLeaf;
  }

  // Public method to call searchPrevBlock with a job j
  public BlockWithStartTime searchPrevBlock(Job j) {
    return searchPrevBlock(j, root, 0);
  } // searchPrevBlock() ends here

  // public method to call searchPrevBlock with a leafNode l
  public BlockWithStartTime searchPrevBlock(LeafNode l) {
    // this will be the block before some j that may be inserted
    // with the earliest due date in block l
    return searchPrevBlock(new Job(l.u));
  } // searchPrevBlock() ends here

  // Public method to call searchNextBlock with a job j
  public BlockWithStartTime searchNextBlock(Job j) {
    return searchNextBlock(j, root, 0);
  } // searchNextBlock() ends here

  // Public method to call searchPrevBlock with a leafNode l
  public BlockWithStartTime searchNextBlock(LeafNode l) {
    // this will be the block after some j that may be inserted
    // with the latest due date in block l
    return searchNextBlock(new Job(l.v));
  } // searchNextBlock() ends here

  // public merges the job j into the block in leafNode l
  public void mergeIntoBlock(Job j, LeafNode l) {
    // if the new job's due date is earlier than block's earliest due date
    if (j.due_date < l.u) {
      // update block's due date
      l.setEarliestDate(j.due_date);
    }
    // overall processing time for block is increased
    l.addProcTime(j.proc_time);
    // update adjustment for rest of tree -
    // right siblings and right siblings of parents recursively

    updateAdjustment(l, j.proc_time);
    insertedLeaf = l;
  } // mergeIntoBlock() ends here

  // public merges the job j into the block in leafNode l
  public void mergeIntoPrevBlock(Job j, LeafNode l) {
    // if the new job's due date is earlier than block's earliest due date
    if (j.due_date > l.v) {
      // update block's due date
      l.setLatestDate(j.due_date);
    }
    // overall processing time for block is increased
    l.addProcTime(j.proc_time);
    // update parent node key values
    TreeNode t = (TreeNode)l.parent;
    updateTree(t);
    while ((t = (TreeNode)t.parent) != null) {
      updateTree(t);
    }
    // update adjustment for rest of tree -
    // right siblings and right siblings of parents recursively
    updateAdjustment(l, j.proc_time);
    insertedLeaf = l;
  } // mergeIntoPrevBlock() ends here

  // public method for merging two blocks together
  public void mergeBlocks(LeafNode prev, LeafNode l) {
    // setting the new earliest date of block l to earliest due date of block prev
    l.setEarliestDate(prev.u);
    // adding processing time to block
    l.addProcTime(prev.p);
    // the block will start when the previous block started
    l.addAdjustment(-prev.p);
    // delete the previous block from the tree
    remove(prev);
  } //mergeBlocks() ends here

  // public method for inserting block into tree
  public void insertNewBlock(Job j, int a) {
    // Let insert to false
    boolean insert = false;
    // make all the variables related to insert have their default values
    split = false;
    insertedLeaf = null;
    // insert the block
    insertBlock(j, a);

    // If the insertion was successful
    if (successfulInsertion) {
      // Increment the size and return success
      size++;
      insert = successfulInsertion;
      successfulInsertion = false;
      // we update the adjustment for the tree -
      // right siblings and right siblings of parent(s) recursively
      updateAdjustment(insertedLeaf, j.proc_time);
    } // if ends here
  } // insertNewBlock() ends here

  // public method removes block corresponding to leafnode l from tree
  public Boolean remove(LeafNode l) {
    // Assign all the variables related to remove to their default values
    boolean delete = false;
    singleNodeUnderflow = false;
    underflow = false;

    // If the element is in the tree
    if (search(l)) {
      // Delete the element
      root = (TreeNode) remove(l, root);
      if (root.parent != null) {
        root.addAdjustment(root.parent.a);
      }
      root.parent = null;
      updateTree(root);

      // If the root has a degree of 1
      if (root.degree == 1 && root.children[0] instanceof TreeNode) {
        // Make root's first children as the root
        root = (TreeNode) root.children[0];
        if (root.parent != null) {
          root.addAdjustment(root.parent.a);
        }
        root.parent = null;
        updateTree(root);
      } // if ends here
    }

    // If the deletion was successful
    if (successfulDeletion) {
      // Decrease the size by 1 and return success
      size--;
      delete = successfulDeletion;
      successfulDeletion = false;
    } // if ends here
    return delete;
  } // remove() ends here

  // This is a public method to call keyOrderList
  public void keyOrderList() {
    // Format the output and call the method
    System.out.println("Keys");
    keyOrderList(root);
    System.out.println();
  } // keyOrderlist() ends here

  // This is a public method to call bfsList
  public void bfsList() {
    // Format the output and call the method
    System.out.print("****************************************");
    System.out.println("**************************************");
    System.out.println("Tree");
    bfsList(root);
  } // bfsList() ends here

  // This method returns the number of the keys stored in the tree
  public int numberOfNodes() {
    // Return the number of values present in the tree
    return size;
  } // numberOfNodes() ends here

  // This is a public method to call height
  public int height() {
    // Return the height of the tree
    return height(root);
  } // height() ends here

} // TwoThreeTree class definition ends here
