import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
/**
Runtime class runs takes the input data and runs the SPT algorithm on
these datasets, generating data for the size of the datasets, against the
running time to run the algorithm on those data sets in the csv format:
n , running time
n1, t1
n2, t2
.
.
.
nm, tm
Here, we have m different collections of jobs, with the size of each collection
(n column) and the running time values separated by a comma
**/
public class Runtime {
  public static void main(String[] args) throws IOException {
    SPT spt;
    BufferedReader reader = new BufferedReader(new FileReader("../data/input.dat"));
    PrintWriter writer = new PrintWriter("SPT_running_time.csv", "UTF-8");
    String line = reader.readLine();
    ArrayList<Integer> processing_times = new ArrayList<Integer>();
    ArrayList<Integer> due_dates = new ArrayList<Integer>();

    // write the heading for the output csv file
    writer.println("Size (n), Running Time");

    // read the input file data
    while (line != null) {
      if (line.equals("***")) {
        // System.out.println("\n---DATASET---" + dataset + "---\n");
        // we have finished retrieving data for this collection of jobs
        // run the algorithm, time it and write the output to a csv file

        // convert collected data to arrays
        int[] p = new int[processing_times.size()];
        // p = processing_times.toArray(p);
        int[] d = new int[due_dates.size()];
        // d = due_dates.toArray(d);

        Iterator<Integer> iterator_p = processing_times.iterator();
        Iterator<Integer> iterator_d = due_dates.iterator();
        for (int i = 0; i < p.length; ++i) {
            p[i] = iterator_p.next().intValue();
            d[i] = iterator_d.next().intValue();
        }

        // initialise the algorithm
        spt = new SPT(p, d);
        // time and run algorithm
        long startTime = System.nanoTime();
        spt.run();
        long endTime = System.nanoTime();
        long duration = (endTime - startTime); // time in ms

        // write results to file
        writer.print(p.length + ","); // size of collection
        writer.println(duration);

        // reinitialise processing times and due dates for the next collection
        // of jobs
        processing_times = new ArrayList<Integer>();
        due_dates = new ArrayList<Integer>();
      }
      else {
        // we have an entry of data of the form p,d
        processing_times.add(Integer.valueOf(line.split(",")[0]));
        due_dates.add(Integer.valueOf(line.split(",")[1]));
      }
      // read the next line
      line = reader.readLine();
    }
    writer.close();
    reader.close();
  } //end of main
} //end of Runtime class
