import java.util.Random;
import java.io.PrintWriter;
import java.io.IOException;
import java.security.SecureRandom;

/**
GenData class generates data file containing processing times and due
dates for jobs in the following file format
.
.
.
***
p11, d11
p12, d12
.
.
.
p1k, d1k
***
p21, d21
p22, d22
.
.
.
p2l, d2l
***
.
.
.
i.e. processing time and due date for a job are comma separated in a row
and collections of jobs (above we have k jobs in one collection and l jobs
in the next collections are separated by '***' delimiters
**/
public class GenData {
  public static void main(String[] args) throws IOException {
    SecureRandom rand = new SecureRandom();
    PrintWriter writer = new PrintWriter("input.dat", "UTF-8");
    // the processing and due date values
    int p = 0;
    int d = 0;
    for (int n = 4; n <= 2000000; n += 2000) {
      // generate n processing and due date values
      for (int i = 0; i < n;  ++i) {
        // generate random processing time between 1 and 10*n
        while (p < 1) {
          p = rand.nextInt(10) + 1;
        }
        // generate random due date with minimum value 10np, and maximum 20*n
        while (d < n) {
          d = rand.nextInt(n) + n + p;
        }

        // print the processing time and respective due date
        writer.print(p + ",");
        writer.println(d);
        p = -1;
        d = -1;
      } //end inner for
      // signify we have a new collection of jobs
      writer.println("***");
    } //end outer for
    writer.close();
  } //end of main
} //end of GenData class
