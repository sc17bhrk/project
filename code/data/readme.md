## Data Generation

To create some smaller test data for verification in the testing directory,
run with

      $ javac GenDataSmallTest.java

      $ java GenDataSmallTest

To create large data (~10GB) for the running time test, run

      $ javac GenData.java

      $ java GenData

The number of test data instances and sizes can be modified in the two
files mentioned above.
